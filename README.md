# Transfer Money API

A coding task for HSBC

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

### Prerequisites

```
Java SDK 
Note you’ll need the tools.jar. Which is missing in later versions of the JDK

MySQL Server 5 or above
```

### Installing

Pull the following git repository `https://gitlab.com/smart-drop-public/client/transfer-money-api/tree/master `  

MySQL
Set up the database to store account information 
1. Create a user `springuser` with a password `ThePassword`
2. Use the sql script accounts_db.sql in the `/resources` folder to build and populate the script
3. You can override the user, password or database  settings in the `application.properties` file in /resources folder 

To run the application use
`gradelew build`

### Running the tests

To test the application you can use postman or Junit:

### Using postman

The test cases [here](HSBC'.postman_collection.json)

### Using JUnit

Note: These tests are incomplete
`gradelew test`

## Misc Notes
Swagger API [Here](https://app.swaggerhub.com/apis/PeterMekkelholt/sample-account/1.0.0#/)
