package hello;

import java.util.concurrent.atomic.AtomicLong;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;

@RestController
public class GreetingController {

    //@Value("${message.default}") private String msg; 

    private static final String template = "Hello, %s!";
    private static final String template_put = "Hello PUT, %s!";
    private final AtomicLong counter = new AtomicLong();


	@RequestMapping(value="/greeting", method=RequestMethod.GET)
    public Greeting greeting(@RequestParam(value="name", defaultValue="World") String name) {
 
        return new Greeting(counter.incrementAndGet(),
                            String.format(template, name));
    }

	@RequestMapping(value="/greeting", method=RequestMethod.PUT)
    public Greeting greeting_put(@RequestParam(value="name", defaultValue="World-PUT") String name) {
        return new Greeting(counter.incrementAndGet(),
                            String.format(template_put, name ));
    }

}