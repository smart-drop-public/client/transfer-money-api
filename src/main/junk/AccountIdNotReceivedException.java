package hello;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;

public class AccountIdNotReceivedException extends RuntimeException {

    public AccountIdNotReceivedException () {
        super("No account id was received with the request");
    }

}