package hello;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;

public class AccountNotFoundException extends RuntimeException {

    public AccountNotFoundException(int id) {
        super("Account id not found : " + id);
    }

}