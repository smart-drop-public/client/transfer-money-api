package hello.errors;

public class EntityNotFoundException extends Exception {

    public EntityNotFoundException(int account_id) {
        super("Account id: " + account_id +" was not found in the database." );
    }
}