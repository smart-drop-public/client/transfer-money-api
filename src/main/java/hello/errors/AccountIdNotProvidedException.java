package hello.errors;

public class AccountIdNotProvidedException extends Exception {

    public AccountIdNotProvidedException(String account_id) {
        super("No account id was provided or the account_id:"+account_id+" isn't valid." );
    }
}
