package hello.models;

public class Transaction{

    private double amount;
    private String datetime;

    public Double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}
}