package hello;

import org.springframework.data.repository.CrudRepository;
import hello.models.Account;

public interface AccountRepository extends CrudRepository<Account, Integer> {
}