package hello;

import hello.errors.AccountIdNotProvidedException;
import hello.errors.EntityNotFoundException;
import hello.models.Account;
import hello.models.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

@RestController
@RequestMapping(path="/account")
@Validated
public class AccountController {
	@Autowired 
	private AccountRepository accountRepository;

	/** getAccountById returns an account from the account_id
	 *
 	 * @param account_id
	 * @return ResponseBody json string of account
	 */
	@RequestMapping(path="/{account_id}", method=RequestMethod.GET)
	public @ResponseBody Optional<Account> getAccountById(
			@PathVariable(value="account_id") int account_id) {
		return accountRepository.findById(account_id);
	}

	/** updateAccountCallback updates account balance. This version
	 * 	Updates and calls the GET API /account/[account_id] to get the updated account object
	 * 	As requests in the coding tasks requirements
	 * @param account_id
	 * @param trans
	 * @return
	 */
	@RequestMapping(path="/{account_id}", method=RequestMethod.PUT)
	public @ResponseBody Account updateAccountCallback(
			@PathVariable(value="account_id") int account_id, @RequestBody Transaction trans) {

		// Update account
		Account account = accountRepository.findById(account_id).get();
		account.updateBalance(trans.getAmount());
		Account savedAccount = accountRepository.save(account);

		// Call API to get updated data
	    final String uri = "http://localhost:8080/account/"+account_id;
	    RestTemplate restTemplate = new RestTemplate();
    	Account respAccount = restTemplate.getForObject(uri, Account.class);
    	return respAccount;
	}


	/** updateAccount is the preferred method
	 * 		updates account and repopulates object before returning the account object
	 *
	 * @param account_id
	 * @param trans
	 * @return
	 */
	@RequestMapping(path="/update/{account_id}", method=RequestMethod.PUT)
	public @ResponseBody Optional<Account> updateAccount(
			@PathVariable(value="account_id") int account_id, 
			@RequestBody Transaction trans) {

		// Update account & save
		Account account = accountRepository.findById(account_id).get();
		account.updateBalance(trans.getAmount());
		Account savedAccount = accountRepository.save(account); 

		// Reload with new data and return
		return accountRepository.findById(account_id);	
	}

	/* AccountIdNotReceivedException method to catch calls with no account_id
	 */
	@RequestMapping(value = "/validate/")
	public ResponseEntity<Account> AccountIdNotReceivedException() throws AccountIdNotProvidedException {
		throw new AccountIdNotProvidedException("");
	}

	/* validateTest: applies error handling
	 * WARNING: when passing an account number that doesn't exist accountRepository.findById
	 * throws an NoSuchElementException
	 *
 	 * @param account_id
	 * @return ResponseBody json string of account
	 */
	@RequestMapping(value = "/validate/{account_id}")
	public @ResponseBody Optional<Account> validateTest(@PathVariable(value="account_id") int account_id) throws EntityNotFoundException {
		Account account = accountRepository.findById(account_id).get();
		if (account == null){
			throw new EntityNotFoundException(account_id);
		}
		return accountRepository.findById(account_id);
	}

}